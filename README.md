# Tersmenu
Easily create menus in toml for rofi, fzf, etc...

# Usage
It is possible to generate an example menu.toml file with ``tersmenu --example`` which can be used for reference.

When tersmenu has read the menu.toml data it will listen for a menu selection from stdin.

Tersmenu can generate a menu both from stdin, or from a menu.toml file by using -c.

When passing toml data through stdin it should be followed by [[end]] so tersmenu knows when to start listening for a menu selection.

Tersmenu can also output all the menu options for a given menu by passing -l.

# Example
Please read [example toml](https://gitlab.com/unic0rn9k/tersmenu#example-menutoml) first

First generate an example menu by running
```bash
tersmenu --example > "example_menu.toml"
```
Then you can make a menu with fzf by running
```bash
tersmenu -c "example_menu.toml" -l | fzf | tersmenu -c "example_menu.toml"
```

# Install
```bash
cargo install --git https://gitlab.com/unic0rn9k/tersmenu.git
```

# Example menu.toml

```toml
[[entry]] # in this case all io flags will default to true
name = 'echo'
echo = 'this will print out without starting a new process'
exec = 'echo this will print out and start a new process'

[[entry]]
name = 'test name'
exec = 'ls'

[entry.io_flags]
stdout = true # all other options will default to false when the io_flags option is set

[[entry]] 
name = 'testy'
exec = 'echo this will not print'
echo = 'this will still print'

[entry.io_flags]
stdout = false
stderr = false
stdin = false
```
