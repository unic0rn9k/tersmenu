#[macro_use]
extern crate clap;

use std::io::{prelude::*,self};
use std::collections::hash_map::HashMap;
use toml;
use color_backtrace;
use std::fs::File;
use std::process::exit;
use serde_derive::*;
use std::process::{Command, Stdio};


#[derive(Debug, Clone, Deserialize, Serialize)]
struct IOFlags{
    stdin: Option<bool>,
    stdout: Option<bool>,
    stderr: Option<bool>,
}

#[derive(Debug, Clone, Deserialize, Serialize)]
struct MenuEntry{
    name: String,
    echo: Option<String>,
    exec: Option<String>,
    io_flags: Option<IOFlags>,
}

#[derive(Debug, Clone, Deserialize, Serialize)]
struct Menu{
    entry: Vec<MenuEntry>,
}

fn main() {
    color_backtrace::install();
    let matches = clap_app!(tersmenu =>
        (version: "0.1.0")
        (author: "unic0rn9k")
        (about: "Lets you easily write small scripts for fzf and rofi in toml")
        (@arg CONFIG: -c --config +takes_value "Sets a config file to use as menu instead of stdin")
        (@arg KEYS: -l --list "Just list menu entries and quit")
        (@arg EXAMPLE: --example "Print an example and exit")
    ).get_matches();

    if matches.is_present("EXAMPLE"){
        println!(r"[[entry]] # in this case all io flags will default to true
name = 'echo'
echo = 'this will print out without starting a new process'
exec = 'echo this will print out and start a new process'

[[entry]]
name = 'test name'
exec = 'ls'

[entry.io_flags]
stdout = true # all other options will default to false when the io_flags option is set

[[entry]] 
name = 'testy'
exec = 'echo this will not print'
echo = 'this will still print'

[entry.io_flags]
stdout = false
stderr = false
stdin = false");
        exit(0)
    }

    let mut menu: HashMap<String, MenuEntry> = HashMap::new();

    let mapify = |menu: Menu|
        menu.entry
        .iter()
        .map(
            |entry|(entry.name.clone(), entry.clone())
        )
        .collect::<HashMap<String, MenuEntry>>();

    match matches.value_of("CONFIG"){
        Some(conf_path) => {
            let mut buffer = "".to_string();
            File::open(conf_path).unwrap().read_to_string(&mut buffer).unwrap();
            menu = mapify(toml::from_str(&buffer).unwrap());
        },
        None => {
            let mut buffer = "".to_string();
            'read: for line in io::stdin().lock().lines(){
                let line = line.unwrap();
                if line == "[[end]]"{
                    menu = mapify(toml::from_str(&buffer).unwrap());
                    break 'read;
                }
                buffer = buffer.to_string() + "\n" + &line;
            }
        }
    }

    if matches.is_present("KEYS"){
        for k in menu.keys(){
            println!("{}",k)
        }
        exit(0)
    }

    let mut entry = "".to_string();
    io::stdin().read_line(&mut entry).unwrap();
    entry.pop();
    let entry = &menu[&entry];
    if let Some(cmd) = &entry.exec{
        let flag = |flag: &Option<bool>| matches!(flag, Some(f) if *f);
            if let Some(io_flags) = &entry.io_flags{
                Command::new("sh")
                    .arg("-c")
                    .arg(cmd)
                    .stdout(if flag(&io_flags.stdout){Stdio::inherit()}else{Stdio::null()})
                    .stderr(if flag(&io_flags.stderr){Stdio::inherit()}else{Stdio::null()})
                    .stdin(if flag(&io_flags.stdin){Stdio::inherit()}else{Stdio::null()})
                    .status()
            }else{
                Command::new("sh")
                    .arg("-c")
                    .arg(cmd)
                    .status()
            }.expect(&format!(
                "failed to execute command for entry:\n {}",
                toml::to_string_pretty(entry).unwrap()
            ));
    }
    if let Some(echo) = &entry.echo{
        println!("{}",echo)
    }
}
